
package copa;

import java.util.ArrayList;

public class Comum extends Usuario {
   
    
    private ArrayList <Macro> listaMacro;
    private ArrayList <Comentario> listaComentarios;
 
    
    
    public Comum() {
    }
    
    public String criarPost(Macro macro) {
        listaMacro.add(macro);
        return "Por favor, aguarde enquando o Administrador avalia seu Subtema";
        
    }
    
    public String criarComentario(Comentario comentario) {
        listaComentarios.add(comentario);
        return "Obrigado, seu comentário foi adicionado com sucesso!";
    }
    
    
}
